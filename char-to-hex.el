;;; -*-  mode: Emacs-Lisp; lexical-binding: t;  -*-

(defun dmcharcode ()
  "Decodes an 8x6 char picture."
  (interactive)
  (forward-line 0)
  (unless (looking-at "^%\\s-+\\([[:alnum:]]+\\)")
    (error "first line should have the character name"))
  (let ((bytes (vector 0 0 0 0 0 0))
        (x1 8) (y1 8) (x2 -1) (y2 -1)
        (xy1 0) (xy2 0)
        (name (match-string-no-properties 1)))
    (dotimes (row 8)
      (forward-line 1)
      (let* ((line (thing-at-point 'line))
             (bits (replace-regexp-in-string "\\`% " "" line)))
        (dotimes (col 6)
          (let ((ch (if (< col (length bits)) (aref bits col) 32))
                (rfb (- 7 row))) ;; row-from-bottom
            (when (= ch ?\#)
              ; update bbox
              (when (< col x1) (setq x1 col))
              (when (< rfb y1) (setq y1 rfb))
              (when (> col x2) (setq x2 col))
              (when (> rfb y2) (setq y2 rfb))
              ; set the bit
              (let ((byte (aref bytes col)))
                (aset bytes col
                      (logior byte (lsh 1 rfb)))))))))
    (when (<= x1 x2)
      (setq xy1 (logior (lsh x1 4) y1))
      (setq xy2 (logior (lsh x2 4) y2)))
    (forward-line 1)
    (when (looking-at (concat "/" name "  <"))
      (let ((a (point)))
        (forward-line 1)
        (delete-region a (point))))
    (insert "/" name "  <" (format "%02X%02X " xy1 xy2)
            (mapconcat (lambda (b) (format "%02X" b)) bytes " ") ">  def\n")))


(defun dmmodel ()
  (interactive)
  (dotimes (n 8)
    (insert "% ......\n")))
